import configparser
import os

import rpy2.robjects as robjects

def generate_config_abstractions():
    current_path = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))

    # Parser del archivo de configuración
    scripts_config = configparser.ConfigParser()
    scripts_config.read(os.path.join(current_path, "conf", "servidor.config"))

    # Transform configparser to dict of dicts:
    config = {}
    for key in scripts_config.keys():
        config[key] = dict(scripts_config[key])

    endpoints = [seccion for seccion in scripts_config.sections() 
                 if seccion not in ["servidor"]]

    # Precargar los ejecutables de R en memoria,
    # para no tener que leerlos con cada ejecución:
    for endpoint in endpoints:
        ejecutable = open(os.path.join(os.path.join(current_path, "R"), config[endpoint]["ejecutable"]), "r")
        config[endpoint]["ejecutable"] = ejecutable.read()
        ejecutable.close()

    ## Precargar el codigo R de warm-up:
    for endpoint in endpoints:
        if config[endpoint]["warm-up"] != "null":
            warm_up = open(os.path.join(os.path.join(current_path, "R"), config[endpoint]["warm-up"]), "r")
            config[endpoint]["warm-up"] = warm_up.read()
            # Ejecución del warm-up:
            robjects.r(config[endpoint]["warm-up"])
            warm_up.close()

    return (config, endpoints)