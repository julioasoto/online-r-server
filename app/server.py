
import json

from confparser import generate_config_abstractions
from middlewares import ResponseLoggerMiddleware

import falcon
import numpy as np
from pandas import DataFrame
import rpy2.robjects as robjects

## FUNCIÓN PRINCIPAL
#  factory function que genera todos los endpoints
#  para la aplicación, acorde al archivo de
# configuración:
def class_factory(endpoint, config_dict):
    if config_dict["metodo"] == "GET":
        if config_dict["tipo"] == "script":
            class Procesado(object):
                def on_get(self, req, resp):
                    #resultado_R = robjects.r(config_dict["ejecutable"])
                    #resp.body = str(np.array(resultado_R))
                    if req.content_type != 'application/json':
                        raise falcon.HTTPUnsupportedMediaType('JSON please!')
                    resp.body = '{"Hola":"Mundo!"}'
                    resp.content_type = config_dict["devuelve"]
                    resp.status = falcon.HTTP_200

    elif config_dict["metodo"] == "POST":
        if config_dict["tipo"] == "funcion":
            class Procesado(object):
                def on_post(self, req, resp):
                    definiciones_R = robjects.r(config_dict["ejecutable"])
                    funcion_R = robjects.r[config_dict["nombre-funcion"]]
                    resultado_R = str(np.array(funcion_R(req.stream.read().decode("utf-8")))[0])
                    resp.body = resultado_R
                    resp.content_type = config_dict["devuelve"]
                    resp.status = falcon.HTTP_200
    else:
        class Procesado(object):
            pass
    return Procesado()

# Creamos la app:
app = falcon.API(middleware=[ResponseLoggerMiddleware()])

# La configuramos:
config, endpoints = generate_config_abstractions()

# Añadimos los endpoints a la app:
for endpoint in endpoints:
    app.add_route(endpoint, class_factory(endpoint, config[endpoint]))
