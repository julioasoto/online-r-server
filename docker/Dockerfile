FROM ubuntu:16.04
MAINTAINER Julio <jsoto@vodafone.com>

# Basics
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN locale-gen es_ES.UTF-8
ENV LANG es_ES.UTF-8
ENV LANGUAGE es_ES:es
ENV LC_ALL es_ES.UTF-8

# User and system packages
RUN adduser --disabled-password --gecos "" yo
RUN echo yo:BigData16 | chpasswd --crypt-method=SHA512
RUN apt-get update && apt-get -y upgrade && apt-get install -y \
	python3 \
	python3-dev \
	python3-pip \
	build-essential \
	sudo \
	r-base \
	r-base-dev \
	supervisor \
	nginx

RUN pip3 install virtualenv

# App dir
USER yo
WORKDIR /home/yo
COPY online-r-server online-r-server
COPY install_r_packages.R .

# Permission changes
USER root
RUN chown -R yo online-r-server
RUN chmod -R 777 /usr/local/lib/R/site-library

# Python and R packages
USER yo
RUN virtualenv -p /usr/bin/python3 python3_env
RUN Rscript install_r_packages.R
RUN source python3_env/bin/activate && pip install -r online-r-server/requirements.txt && pip install numpy && pip install pandas && pip install setproctitle

# Supervisor
USER root
COPY gunicorn_supervisor.conf /etc/supervisor/conf.d/gunicorn_supervisor.conf
USER yo
RUN touch /home/yo/online-r-server/log/gunicorn_supervisor.log && \
    mkdir -p /home/yo/online-r-server/run

# Nginx
USER root
RUN service nginx stop
COPY online_r_server /etc/nginx/sites-available/online_r_server
RUN ln -s /etc/nginx/sites-available/online_r_server /etc/nginx/sites-enabled/online_r_server
RUN sed  -i '1i daemon off;' /etc/nginx/nginx.conf
RUN rm /etc/nginx/sites-enabled/default

# Run
EXPOSE 80
CMD ["/usr/bin/supervisord"]
