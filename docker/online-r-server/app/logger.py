import configparser
import logging
import os

def parse_logging_conf(path, app_logger):
	config = configparser.ConfigParser()
	config.read(os.path.join(path, "conf", "logging-app.config"))

	for handler in config["handlers"]["keys"].split(","):
		if config["handler_" + handler]["class"] == "FileHandler":
			project_dir = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))
			logfile_entry = config["handler_" + handler]["logfile"]
			if "$PROJECT_DIR" in logfile_entry:
				logfile_location = logfile_entry.replace("$PROJECT_DIR", project_dir)
			else:
				logfile_location = logfile_entry
			new_handler = logging.FileHandler(logfile_location)
			if config["handler_" + handler]["level"] == "DEBUG":
				new_handler.setLevel(logging.DEBUG)
			elif config["handler_" + handler]["level"] == "INFO":
				new_handler.setLevel(logging.INFO)
			elif config["handler_" + handler]["level"] == "WARNING":
				new_handler.setLevel(logging.WARNING)
			elif config["handler_" + handler]["level"] == "ERROR":
				new_handler.setLevel(logging.ERROR)
			elif config["handler_" + handler]["level"] == "CRITICAL":
				new_handler.setLevel(logging.CRITICAL)

			new_handler.setFormatter(logging.Formatter(config.get("formatter_" + config["handler_" + handler]["formatter"], "format", raw=True)))
			app_logger.addHandler(new_handler)


current_path = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))

logger = logging.getLogger(__name__)

# Parser de DEBUG true o false; que no implica que los logs
# de debug se escriban en archivo; para ello está
# parse_logging_conf:
general_config = configparser.ConfigParser()
general_config.read(os.path.join(current_path, "conf", "falcon.config"))
try:
	if general_config["general"]["debug"] == "True":
		logger.setLevel(logging.DEBUG)
except KeyError:
	pass

parse_logging_conf(current_path, logger)

## Utilidad para imprimir mensajes de la
#  entrada y salida de los endpoints:
def io_log(message, endpoint, input, output):
    return " - ".join([str(data) for data in [message, endpoint, input, output]])

