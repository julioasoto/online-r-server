from logger import logger, io_log

# Logging middleware:
class ResponseLoggerMiddleware(object):
    def process_response(self, req, resp, resource, req_succeeded):
        logger.debug(io_log("", req.relative_uri, req.stream.read().decode("utf-8"), resp.body))