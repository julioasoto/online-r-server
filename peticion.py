# Con cURL:
# curl -H "Content-type: application/json" -X POST http://127.0.0.1:5000/predict -d '{"data":[{"a":1, "b":2}, {"a":3, "b":4}]}'

# Benchmark con siege:
# siege -H 'Content-Type: application/json' -c 100 -t60S 'http://127.0.0.1:7005/modelo_r_02 POST {"data":[{"a":1, "b":2}, {"a":3, "b":4}]}'
# siege -c 100 'localhost:7005/modelo_r_01'

import requests

r = requests.post('http://127.0.0.1:7005/modelo_r_03', json = {"data":[{"a":1, "b":2}, {"a":3, "b":4}]})


